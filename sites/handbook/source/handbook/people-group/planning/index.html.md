---
layout: markdown_page
title: "People Group Planning"
---

People Operations Specialists will manage a monthly People Group calendar to highlight important dates.  Examples would include Summit dates, compa group changes, merit or compensation program dates, manager, etc. Team member training dates can be found on the [training calendar](/handbook/people-group/learning-and-development/#learning-sessions) 


## 2020 People Group Planning and Companywide Events
- **January 9th** - People Team OKR Planning
- **February** - Inc Best Place to Work Survey
- **February 1st** - Annual Comp Review take effect 
- **March TBD** - Great Places to Work Certification Survey
- **April 3rd** - 360 Feedback 
- **March 22nd-27th** GitLab Contribute **CANCELLED/POSTPONED**
- **April TBD** - Engagement Survey
- **April TBD** - People team OKR Setting
