---
layout: handbook-page-toc
title: "People Experience Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# People Experience Team

This page lists all the processes and agreements for the People Experience team. 

If you need our attention, please feel free to ping us on issues using `@gl-people-exp`.

## People Experience Team Availability

Holidays with no availability for onboarding/offboarding/career mobility issues:

| Date                        |
|-----------------------------------|
| 2020-05-01 | Family & Friends Day
| 2020-08-14 | Family & Friends Day
| 2020-12-25 |
| 2021-01-01 |

### OOO Handover Process for People Experience Team

1. The People Experience Associate will create a document the day before their scheduled OOO with the following:
- What urgent tasks are there that need to be reassigned:
* Onboarding
* Offboarding
* Career Mobilities
* Any other tasks
- Is there anything else that we should be made aware of and contribute to whilst you are away?
2. The People Experience Coordinator will then reassign tasks to an alternative People Experience Associate.
3. Get assistance from the People Operation Specialist team if additional hands are needed.

### OOO Process for People Experience Coordinator

1. When the People Experience Coordinator is due to be OOO, the People Experience Associates will allocate the relevant tasks between themselves.
2. In the event that there are items that need urgent attention whilst the People Experience Coordinator is OOO, the Coordinator will hand over the relevant tasks to the People Experience Team Lead or People Experience Associates.

## People Experience Team Processes

### Weekly Rotations 

The People Experience Team are currently trialling a task rotation on a weekly basis. The allocation tracker can be found in the [People Exp/Ops Tracker]. This will initially be trialed on a weekly rotation and then possibly moved to bi-weekly and monthly. 

Certain tasks will be allocated based on a ongoing rotation between all People Experience Associates. 

The following factors will also be taken into consideration:

- Scheduled PTO for the team members
- Ensure that the tasks split evenly and fairly 
- A tracker will be kept to track the data of how the tasks are split up

**The Rotation groups are listed as follows:**

- Probation Periods / Gift Requests / New Hire Swag Email
- BHR Onboarding Report / Moo Report / Allocations for Onboarding, Offboarding, Mobility

- I-9 Administration / Pruning of Stale Records (TBD)

**The following tasks will be completed by any available People Experience Associate with the designated SLA:**

- Letters of Employment / VOE 
- Anniversary Queries

#### Allocations for Onboarding

- We always try and split evenly and fairly.
- Not time zone specific to ensure that all Associates learn all aspects to different countries during onboarding. 

#### Allocations for Offboarding

- Team has 12 hours to create and notify of the offboarding issue.
- The person that is in the allocation rotation will also add the offboarding to the PEA team calendar.
- Due to different time zones, the offboarding issue creation and task completion can be tagged team by the Associates. 

#### Allocations for Career Mobility 

- We always try and split evenly and fairly. 

### Audits 

There are certain tasks that need to be completed by all the different departments and team members and we as the People Experience Team need to ensure to remain compliant in line with these tasks. Herewith a breakdown of the important compliance and team tasks:  

- Onboarding

This will be the responsibility of the People Experience Associate that is assigned to the specific team members onboarding. 

    - Ensure that the new team member has shared a screenshot of their FileVault disk encryption and computer serial number in the onboarding issue. 
    - Ensure that the new team member has acknowledged the 'Handbook' compliance task in the onboarding issue. 
    - Ensure that the manager tasks are completed prior to and after the new team member has started. 
    - Ensure that an onboarding buddy has been assigned to assist the new team member. 
    - Ping the relevant team members to call for a task to be completed in the onboarding issue.

- Offboarding

This will be the responsibility of the People Experience Associate that is assigned to the specific team members offboarding. 

    - Ensure that all the different departments complete their tasks within the 5 day due date. 
    - Immediate action is required from the People Experience Team and the IT Ops Team once the issue has been created. 
    - Ping the relevant team members to call for a task to be completed in the offboarding issue.
    - Confirm that a departure announcement has been made in #team-member-updates on Slack.

- Career Mobility 

This will be the responsibility of the People Experience Associate that is assigned to the specific team members career mobility issue. 

    - Check to see whether the team member that has migrated needs any guidance.
    - Ensure that the previous manager and current manager completes their respective tasks. 
    - The issue should be closed within 2 weeks of creation, ping the relevant team members to call for a task to be completed in the issue.

**Over and above these audits, the Compliance Specialist will perform their own audits to ensure that certain tasks have been completed.**

### Pulling of BambooHR Onboarding Data

Every Monday and Wednesday, the Associate in the rotation will pull the report that has been shared in BambooHR called `New Hires`. The data for the next 2 weeks will be added to the spreadsheet to ensure sufficient time in completing the pre-onboarding tasks. 

### Weekly Moo Report

Every week, the People Experience Associate in the rotation creates a Report in BambooHR containing all Active Employees in three columns: their First Name, their Last Name, their GitLab work email. People Experience Associate adds this data to the `GitLab : Moo` spreadsheet in a new weekly tab titled that Tuesday's date (format yyyy-mm-dd) so that our Moo Rep can grab this data and send out email invitations to new team members from the Moo platform.

Please note that all info from the report is added to the sheet, including any past hires. The Moo system will only send the invitation to the new members not previously listed on the sheet. This ensures that any potential missed members still receive their invitation.

### Letters of Employment and Employee Verification Requests

This lists the steps for the People Experience team to follow when receiving requests:

#### Letters of Employment 

Once the team member completes the form as listed in the [Handbook](https://about.gitlab.com/handbook/people-group/frequent-requests/#letter-of-employment), we will receive an email in the People Experience Inbox to view and audit if the information has been correctly inserted by the team member. 
- If the information is correct, please proceed with forwarding the Letter of Employment to the team member by email. 
- If the information is incorrect, please email the team member and provide the details of what is incorrect and ask that they resubmit the form with the correct information. 

#### Employee Verifications

We may be contacted by different vendors who require a team members employment to be verified along with other personal information. 

Most importantly, check to see whether authorisation has been received from the team member that we may provide the personal information. If no authorisation form is attached, make contact with the team member via email or Slack to get the required consent. 
- If the vendor is asking for general information in the email, simply respond back to the email with the requested information. 
- If the vendor is requesting a form to be completed, complete the form via HelloSign and encrypt the document before sending via email. 
- If the vendor is requesting a form to be completed for a US team member, forward this request on to the US Payroll team to have completed. 
- If you need additional figures that we do not have access to, send a message in the `payroll-peopleops` Slack channel to request the information needed to complete the form. 

In some instances, we may be contacted by certain Governmental institutions asking for clarity into termination / seperation reasons and agreements of a team member. Please forward these emails to the relevant People Business Partner that submitted the offboarding notification, as they will have full context into the reasons and agreements and will choose to respond if needed. 

### Probation Period Process

Every week, the Associate assigned to the allocation will send emails to the team members respective managers to get the relevant approval that they are satisfied with the team members performance. **It is important to note that if a response was not received by a manager in the previous allocated rotation, this will fall onto the Associate in the rotation in the next week to follow up with the manager again.** 

Fortunately, we are able to use Mail Merge to get the emails sent out. Please see the below steps to follow:

1. Pull the report `Probation Period Audit` from BambooHR as a CSV and import to shared People Exp drive, under the Probation Period folder. 
1. Copy the data from Column A - I, this is important to ensure that the formulas are not replaced in Columns K - M. 
1. Pull up the main Probation Period [spreadsheet](https://docs.google.com/spreadsheets/d/1v631AN0x2zgg-TLrE41yGEyK550bHh53suHSyIN09gk/edit?ts=5f296351#gid=0)
1. Paste the copied data from the BambooHR sheet into `Probation Period` sheet.
    - United Kingdom: 
        - Probation periods differ between 3 and 6 months.
        - If hired before 2020-05-01, then 3 months probation is applicable. If hired after 2020-05-01, a 6 month probation period is applicable. 
        - It will say “Check Hire Date”, we have to add 3 months +91 or +182 in the formula
1. Go to Column K (notification send date), then select filter by condition > and then date is after > today > then select OK.
1. Then sort by A-Z under Column K again using the filter. 
1. Add new sheet at the bottom with the date the import is taking place (like with Moo) and only copy the team members that would have been sent from that week to that spreadsheet. This will also include the sending of all team members in that same week (in rotation).
1. Go to Add-ons > Document Studio > Open
1. Click on `Mail Merge with Gmail`
1. Edit with visual editor
1. Check to ensure that the template is the Manager’s email, which can also be found in People Exp folder in Google Drive. 
1. Ensure the `Send email to` option is listed as `Supervisors email`
1. Edit subject line, it should be listed as `Probation Period Ending Soon`
1. Ensure to update the merge fields in the template (no manager names will be inserted) > edit team member name and probation period end date.
1. Add your signature 
1. Go to “finish and merge”
1. You are able to preview the template at the same time (will be sent to private email).
1. If all good, click on save, this will automatically send the email to the manager.  
1. Copy and paste the team member into `People Exp/Ops Tracker` for tracking purposes.
1. If no response is received from previous week, send mail merge again to those managers using the same template (it's totally okay that we are repeating ourselves). You will need to delete the document studio file column data if you would like to resend in Columns N - P. 

### OSAT Team Member Feedback

Once a new team member has completed their onboarding, they are asked to complete the `Onboarding Survey` to share their experience. Based on the scores received, the People Experience Associate assigned to the specific team members onboarding, will complete the following:

- Score of 4 or higher: use own discretion based on the feedback received and see whether there are any improvements or changes that can be made to the onboarding template / onboarding process (this can be subjective). 
- Score of 3 or lower: reach out to the team member and schedule a feedback session to further discuss their concerns and feedback provided. 

#### Onboarding Buddy Feedback

In the same survey, new team members are able to provide a score and feedback on their onboarding buddy. If the score and feedback received is constructive and valuable insights when the score is low, the People Experience Associate assigned to that specific team members onboarding, should reach out to the manager of the onboarding buddy and provide feedback in a polite and supportive way.  

### Code of Conduct Rotation

A monthly report will be pulled from BambooHR for `Code of Ethics Acknowledgment 2020` by the Associate in the respective rotation to check that all pending team member signatures have been completed. If it has not been signed by the team member, please select the option in BambooHR to send a reminder to the team member to sign. Please also follow up via Slack and ask the team member to sign accordingly. If there are any issues, please escalate to the People Experience Team Lead for further guidance. 

### Anniversary Period Gift Queries

*Current Process*

Team members celebrating their first, third and fifth anniversaries with GitLab would have received an email from the People Experience Team asking them to complete their information in order to have their item shipped. Due to production and shipping delays, many team members celebrating their anniversaries prior to June 2020 are still waiting for their gifts and are instructed to complete the `First Anniversary Gifts` [form](https://docs.google.com/forms/d/e/1FAIpQLScgBaNWmqEA5Tx2jZlffiDIeOfn7fvyyy7oF7cXQrSq9saYvQ/viewform). Once we receive a request on the form:

1. Check to see whether the team members are listed on the current `Orders Backordered Datasheet` in Google Drive.
    - If the team member's name is not included on there, we can take this as confirmation that an order was never received for the team member. The team member can then either proceed with ordering the existing Tanuki socks in the Swag store or wait for the new socks to be designed and produced. 
    - If the team member's name is on the backlog to be shipped out, this should be shipped out soon, but may have some delays with reaching the team members due to the pandemic. 
1. Advise the team member how they should proceed based on the above (either place order for Tanuki socks, wait for new design, or advise their shipment will arrive soon).  

*New Process*

With the new sock design being released soon for first year celebrants, the process will be different to the current process and will be updated soon! 

### Gift Requests

When a team member completes the GitLab gift form request, the People Experience team receives an email to people-exp@gitlab.com to process the request. Most often, these are requests for flowers to be sent to another team member. Please see the below steps for guidance on how to process these requests:

- Navigate and open the gift form requests in `Google spreadsheet`. 
- Open the PeopleOps 1Password Vault and select `Gift & Flower Vendors` to gain access to the various vendors used. 
- Place order and once confirmed, add data, including order confirmation link, to spreadsheet. 
- Send the requesting team member an email or message in Slack to confirm that you have processed the request/order. 
- Use the Gift [page](https://about.gitlab.com/handbook/people-group/#gifts) in the Handbook for any further information regarding the policy for gift order requests. 
- If a gift card is needed for a particular event or prize, create an issue for the Payroll team to get the relevant approval. 

### Scheduling Group Conversations 

The People Experience team assists with the administration of the GitLab Team Calendar and automatically can assist with the following:

- Scheduling new Group Conversations or AMA's
- Assist with updating the calendar when Group Conversations are rescheduled

Here are the steps to follow when scheduling a Group Conversation or AMA:

1. Log into the PeopleOps Shared Zoom Account via Okta
1. Select 'Schedule a New Meeting'
1. Insert the name of the conversation under 'Topic'
1. Below the date and time option, select 'Recurring Meeting'
1. Under the 'Recurrence' dropdown, select 'No Fixed Time'
1. Select the 'Require meeting password' option. This will automatically generate a password for you. 
1. Ensure that the Video is on for both 'Host' and 'Participant'
1. At the bottom of the page, untick 'Enable waiting room' 
1. You are also able to add any alternative hosts in the event that the People Group will not be assisting with moderating the call. 
1. Click 'Save'
1. Once saved, select the option to 'Add to Google Calendar'
1. You will be redirected to the Google Calendar and prompted to select a calendar. Select your own account. 
1. Confirm allowing access for Zoom to Google Calendar.
1. You will now be able to edit the calendar invitation with the following:
    - Date and time (always needs to be scheduled in PST) and the meeting time should be set to 25 minutes. 
    - Add the description and agenda link 
    - Add the Host and Moderator to the description
    - Add to the GitLab Team Meetings
    - Invite everyone@gitlab.com 
1. Click 'Save' 
1. You will be prompted to select whether you would like to send invitation emails to Google Calendar guests? Select 'Don't Send'
1. You will receive a bunch of automated messages in your personal Inbox regarding the scheduled call, you can simply ignore and delete them. 

That's it, you have successfully scheduled the meeting! Now just remember to let the requesting team member know to check that you have completed the request. 

### HelloSign

As the DRI for HelloSign, when a team member needs to have access to sign documents for the company, an access request needs to be created and assigned to the People Experience Team for provisioning. 

#### Process once Access Request is received:

1. Log into [HelloSign](https://app.hellosign.com/home/) using your personal account information
1. Select `Team` on the left hand-side of the page
1. Insert the team members GitLab email address and click `Invite`
1. Take a screenshot of the confirmed invitation sent and upload to the Access Request as confirmation

#### Monthly Billing

When monthly expenses are due, we need to be able to provide Finance with the specific team members name in order to assign to the correct Department. To do this we would need to complete the following steps:

1. Download the invoices from HelloSign which can be found under the 'Billing' section. 
1. Send a request to HelloSign Support on a monthly basis for the specific team members which the costs that have been charged, relates to. Often the response is not received in time for the monthly expenses. 
1. Once the list is received from HelloSign, simply send the Accounts Payable Specialist an email with the relevant team member information.  

### Regeling Internet Thuis form

New team members based in the Netherlands will send an email to people-exp@gitlab.com with the Regeling Internet Thuis form. The People Experience team will then forward this form to the payroll provider in the Netherlands via email. The contact information can be found in the People Ops 1Password vault, under "Payroll Contacts".
