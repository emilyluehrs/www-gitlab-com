---
layout: handbook-page-toc
title: "GitLab Technical Certifications"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Technical Certifications

GitLab offers technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers. 

### Overview

GitLab is planning and developing several technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers. 

### Why certification?

#### For employers
Team managers now have a way to confirm their team members possess the skills needed to effectively use GitLab in their daily DevOps tasks. This helps ensure the team will be able to successfully adopt GitLab and make the most of the organization's investment.

#### For individuals

Individual GitLab users who earn certification receive a certification logo they can share on social media to showcase their accomplishment. This helps highlight to colleagues and employers their proficiency in effectively using the GitLab platform.

### Currently available certifications

Here are the certifications GitLab has soft-launched in FY'21. Each set of certification assessments is currently available to [GitLab Professional Services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/) customers who purchase the related course. Course participants gain access to the certification assessments immediately after completing their course sessions. GitLab is planning to offer asynchronous options for anyone to prepare for and take the certification assessments, with online availability targeted for the end of calendar year 2020.

#### GitLab Certified Associate

- Soft-launched in: May 2020
- Description page: [GitLab Certified Associate](https://about.gitlab.com/services/education/gitlab-certified-associate/)
- Related course: [GitLab with Git Basics](https://about.gitlab.com/services/education/gitlab-basics/)
- Also available at: [GitLab Commit 2020](https://about.gitlab.com/events/commit/)

#### GitLab CI/CD Specialist

- Soft-launched in: July 2020
- Description page: [GitLab CI/CD Specialist](https://about.gitlab.com/services/education/gitlab-cicd-specialist/)
- Related course: [GitLab CI/CD Training](https://about.gitlab.com/services/education/gitlab-ci/)
- Also available at: [GitLab Commit 2020](https://about.gitlab.com/events/commit/)

#### GitLab InnerSourcing Specialist

- Soft-launched in: June 2020
- Description page: [GitLab InnerSourcing Specialist](https://about.gitlab.com/services/education/gitlab-innersourcing-specialist/)
- Related course: [GitLab InnerSourcing Training](https://about.gitlab.com/services/education/innersourcing-course/)


### Planned certifications for FY'22

The following certifications are planned for future development. 

* GitLab System Administration Specialist
* GitLab Project Management Specialist
* GitLab DevOps Professional

